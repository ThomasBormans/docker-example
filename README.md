# Docker Example #

## Requirements ##

* Docker

## Start app ##

```sh
$ npm start
```

## Execute hello command ##

```sh
$ docker exec -it newmedia npm run hello
```

Server is available at [http://localhost:4000](http://localhost:4000).

MongoDB is available at [mongodb://localhost:27018](mongodb://localhost:27018).
