const app = require("express")();
const mongoose = require("mongoose");
const port = 3000;

mongoose.connect("mongodb://mongo/test-db", {
    useMongoClient: true,
});

mongoose.Promise = Promise;

const Test = new mongoose.Schema({
    key: {
        type: String,
    },
});

// Set the name of the collection
Test.set("collection", "test");

const TestModel = mongoose.model("Test", Test);

TestModel
    .create({
        key: new Date().getTime().toString(),
    })
    .then((response) => {
        console.log("Record created");
    });

app.route(["/", "/*"]).all((req, res) => {
    TestModel
        .find()
        .lean()
        .exec()
        .then((response) => {
            res.status(200).json(response);
        })
        .catch((err) => {
            res.status(400).json({
                err: err,
            });
        });
});

app.listen(port, () => {
    process._rawDebug(`Server ready and running on ${port}.`);
});
