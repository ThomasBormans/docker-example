FROM node:boron-alpine

WORKDIR /usr
COPY package.json /usr/package.json
RUN npm install

ENV NODE_PATH=/usr/node_modules

WORKDIR /usr/app

EXPOSE 3000

CMD [ "/usr/node_modules/.bin/nodemon", "server.js" ]